import { Injectable } from '@angular/core';
import {Vehiculo} from './vehiculo';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {dataLogin} from './login/login.component';
import { stringify } from 'querystring';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class VehiculoService {
  private vehiculosUrl = 'https://shielded-beyond-99836.herokuapp.com/vehicles';  // URL a web api
  // inyecta el MessageService en el VehiculoService que se inyecta en el VehiculosComponent.
  constructor(
    private http: HttpClient) {
        /*if (dataLogin == null) {
          this.vehiculosUrl = 'https://shielded-beyond-99836.herokuapp.com/vehicles';
        } else {
          this.vehiculosUrl = 'https://shielded-beyond-99836.herokuapp.com/with_authentication//vehicles';
        }*/  // para login
     }

  getVehiculos(): Observable<Vehiculo[]> {
    // TODO: envía el mensaje _despues_ de ir a buscar a los heroes
    return this.http.get<Vehiculo[]>(this.vehiculosUrl, httpOptions)
    .pipe(
      tap(vehiculos => console.log(`vehiculos buscado`)),
      catchError(this.handleError('getVehiculos', []))
    );
  }
  getVehiculo(id: number): Observable<Vehiculo> {
    const url = `${this.vehiculosUrl}/${id}`;
    return this.http.get<Vehiculo>(url).pipe(
      tap(_ => console.log(`vehiculo id=${id} buscado`)),
      catchError(this.handleError<Vehiculo>(`getVehiculo id=${id}`))
    );
  }

  /** PUT: update the vehicle on the server */
  updateVehiculo (vehiculo: Vehiculo): Observable<any> {
    const url = this.vehiculosUrl + '/' + vehiculo.id;
    // const body = Object.assign(vehiculo, dataLogin);  //para enviar dataAccess con vehiculo
  return this.http.put(url, vehiculo, httpOptions).pipe( // reemplazar vehiculo por body
    tap(_ => console.log(`updated vehiculo id=${vehiculo.id}`)),
    catchError(this.handleError<any>('updateVehiculo'))
  );
}

addVehiculo (vehiculo: Vehiculo): Observable<Vehiculo> {
 // const body = Object.assign(vehiculo, dataLogin);  //para enviar dataAccess con vehiculo
  return this.http.post<Vehiculo>(this.vehiculosUrl, vehiculo, httpOptions).pipe( // reemplazar vehiculo por body
    tap(_ => console.log(`added vehiculo w/ id=${vehiculo.id}`)),
    catchError(this.handleError<Vehiculo>('addVehiculo'))
  );
}

deleteVehiculo (vehiculo: Vehiculo | number): Observable<Vehiculo> {
  const id = typeof vehiculo === 'number' ? vehiculo : vehiculo.id;
  const url = `${this.vehiculosUrl}/${id}`;

  return this.http.delete<Vehiculo>(url, httpOptions).pipe(
    tap(_ => console.log(`deleted vehicle id=${id}`)),
    catchError(this.handleError<Vehiculo>('deleteVehiculo'))
  );
}


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }



}
