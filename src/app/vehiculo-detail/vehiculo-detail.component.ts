import { Component, OnInit, Input } from '@angular/core';
import {Vehiculo} from '../vehiculo';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { VehiculoService } from '../vehiculo.service';

@Component({
  selector: 'app-vehiculo-detail',
  templateUrl: './vehiculo-detail.component.html',
  styleUrls: ['./vehiculo-detail.component.css']
})
export class VehiculoDetailComponent implements OnInit {
  @Input() vehiculo: Vehiculo;

  constructor(
    private route: ActivatedRoute,  // ActivatedRoute contiene información sobre la ruta a esta instancia de VehiculoDetailComponent
    private vehiculoService: VehiculoService, // obtiene datos de héroe del servidor remoto y usará para obtener el héroe a mostrar
    private location: Location // navegue de regreso
  ) { }

  ngOnInit(): void {
    this.getVehiculo();
  }

  getVehiculo(): void {
    const id = +this.route.snapshot. // R.S es una imagen estática de la información de ruta poco después de que se creó el componente.
    paramMap.get('id'); // es un diccionario de valores de parámetros de ruta extraídos de la URL. "id" devuelve el id del héroe.
    this.vehiculoService.getVehiculo(id)
      .subscribe(vehiculo => this.vehiculo = vehiculo);
  }

  delete(vehiculo: Vehiculo): void {
    this.vehiculoService.deleteVehiculo(vehiculo).subscribe(() => this.goBack);
  }

  goBack(): void {
    this.location.back();
  }

}
