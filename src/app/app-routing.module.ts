import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {VehiculosComponent} from './vehiculos/vehiculos.component';
import {VehiculoDetailComponent} from './vehiculo-detail/vehiculo-detail.component';
import {AddVehiculoComponent} from './add-vehiculo/add-vehiculo.component';
import {EditVehiculoComponent} from './edit-vehiculo/edit-vehiculo.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  {  path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'vehiculos', component: VehiculosComponent},
  {path: 'detail/:id', component: VehiculoDetailComponent},
  {path: 'add', component: AddVehiculoComponent},
  {path: 'edit/:id', component: EditVehiculoComponent},
  {path: 'login', component: LoginComponent},
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(routes);
