import { Component, OnInit, Input } from '@angular/core';
import {Vehiculo} from '../vehiculo';
import { Location } from '@angular/common';
import { VehiculoService } from '../vehiculo.service';

@Component({
  selector: 'app-add-vehiculo',
  templateUrl: './add-vehiculo.component.html',
  styleUrls: ['./add-vehiculo.component.css']
})

export class AddVehiculoComponent implements OnInit {
  vehiculo: Vehiculo;
  categorias: string[] = ['motocicleta', 'automovil', 'srv', 'camioneta' , 'camion' , 'micro'];

  constructor(
    private vehiculoService: VehiculoService, // obtiene datos de héroe del servidor remoto y usará para obtener el héroe a mostrar
    private location: Location // navegue de regreso
  ) {this.vehiculo = new Vehiculo(); }

  ngOnInit(): void {
  }


  save(): void {
    this.vehiculoService.addVehiculo(this.vehiculo)
      .subscribe(() => this.goBack());
  }


  goBack(): void {
    this.location.back();
  }
}
