import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { VehiculosComponent } from './vehiculos/vehiculos.component';
import { VehiculoDetailComponent } from './vehiculo-detail/vehiculo-detail.component';
import { VehiculoService } from './vehiculo.service';
import { ROUTING } from './app-routing.module';
import { AddVehiculoComponent } from './add-vehiculo/add-vehiculo.component';
import { EditVehiculoComponent } from './edit-vehiculo/edit-vehiculo.component';
import { Angular2TokenService } from 'angular2-token';
import { CustomFormsModule } from 'ng2-validation'; // https://github.com/yuyang041060120/ng2-validation
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    VehiculosComponent,
    VehiculoDetailComponent,
    AddVehiculoComponent,
    EditVehiculoComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ROUTING,
    HttpClientModule,
    HttpModule,
    RouterModule,
    CustomFormsModule,
  ],
  providers: [
    VehiculoService,
    Angular2TokenService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {

}
