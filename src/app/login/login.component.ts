import { Component, Injectable, OnInit } from '@angular/core';
import {Angular2TokenService} from 'angular2-token';
import {environment} from '../../environments/environment';

interface LoginAccess {
  accessToken: string;
  accessType: string;
  client: string;
  expiry: string;
  uid: string;
}

export let dataLogin: LoginAccess;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  emailLogic = '';
  passwordLogic = '';
  constructor(private _tokenService: Angular2TokenService) {
    this._tokenService.init(environment.token_auth_config);
  }

  ngOnInit() {
  }

  autenticar() {
    this._tokenService.signIn({
      email:    this.emailLogic,
      password: this.passwordLogic,
    }).subscribe(
      res =>       {console.log('auth response:', res);
                    console.log('auth response headers: ', res.headers.toJSON());
                    console.log('auth response body:', res.json()); },
      error =>    console.log(error)
    );
  }
  cerrarsesion() {
    this._tokenService.signOut().subscribe(
      res =>      console.log(res),
      error =>    console.log(error)
    );
  }
  validarToken() {
    this._tokenService.validateToken().subscribe(
      res =>      console.log(res),
      error =>    console.log(error)
    );
  }
}
